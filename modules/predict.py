import json
import logging
import os
from datetime import datetime

import dill
import pandas as pd
from sklearn.pipeline import Pipeline

path = os.environ.get('PROJECT_PATH', '..')


def model_upload():
    get_model = sorted(os.listdir(f'{path}/data/models'))[-1]
    with open(f'{path}/data/models/{get_model}', 'rb') as file:
        model = dill.load(file)
    return model


def get_predictions():
    preds = {
        'car_id': [],
        'price': [],
        'pred': [],
    }
    datatest_cars = os.listdir(f'{path}/data/test')
    model = model_upload()

    for car_id in datatest_cars:
        with open(f'{path}/data/test/{car_id}', 'rb') as file:
            car = json.load(file)

        df = pd.DataFrame(car, index=[0])
        y = model.predict(df)
        preds['car_id'].append(car_id.split('.')[0])
        preds['price'].append(df['price'].loc[df.index[0]])
        preds['pred'].append(y[0])
    return pd.DataFrame(preds)


def predict():
    predictions = get_predictions()
    preds_filename = f'{path}/data/predictions/preds_{datetime.now().strftime("%Y%m%d%H%M")}.csv'
    predictions.to_csv(preds_filename, index=False)
    logging.info(f'Predictions are saved as {preds_filename}')


if __name__ == '__main__':
    predict()
